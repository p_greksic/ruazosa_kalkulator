package lecture4.ruazosa.fer.hr.calculator

import org.junit.Assert
import org.junit.Test

/**
 * Created by dejannovak on 25/03/2018.
 */
class CalculatorUnitTest {

    @Test
    fun test_reset() {
        Calculator.reset()
        Assert.assertEquals(0.0, Calculator.result, 0.0)
        Assert.assertEquals(0, Calculator.expression.count())
        println("test_reset passed!")
    }


    @Test
    fun test_addNumber() {
        Calculator.reset()
        try {
            Calculator.addNumber("Not a number")
            Assert.fail()
        }
        catch (exc: Exception) {
            Assert.assertEquals(exc.message, "Not valid number")
        }
        Calculator.reset()

        try {
            Calculator.addNumber("100.00")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.assertEquals(exc.message, "Not a valid order of numbers in expression")
        }
        Calculator.reset()

        try {
            Calculator.addNumber("100.00")
            Calculator.addOperator("+")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.fail()
        }
        println("test_addNumber passed!")
    }

    @Test
    fun test_addOperator() {
        Calculator.reset()
        try {
            Calculator.addNumber("100.00")
            Calculator.addOperator("+")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.fail()
        }
        Calculator.reset()
        try {
            Calculator.addNumber("100.00")
            Calculator.addOperator("(")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.assertEquals(exc.message, "Not a valid operator")
        }

        Calculator.reset()
        try {
            Calculator.addNumber("100.00")
            Calculator.addOperator("+")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.fail()
        }
        // dodani testovi, testiraj operatora mnozenja, *
        Calculator.reset()
        try {
            Calculator.addNumber("100.00")
            Calculator.addOperator("*")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.fail()
        }

        // dodani testovi, testiraj dodavanje operatora dijeljenja, /
        Calculator.reset()
        try {
            Calculator.addNumber("100.00")
            Calculator.addOperator("/")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.fail()
        }

        Calculator.reset()
        try {
            Calculator.addNumber("100.00")
            Calculator.addOperator("+")
            Calculator.addOperator("-")
            Calculator.addNumber("200.00")
        }
        catch (exc: Exception) {
            Assert.assertEquals(exc.message, "Not a valid order of operator in expression")
        }
        println("test_addOperator passed!")
    }

    @Test
    fun test_evaluate() {
        Calculator.reset()
        Calculator.addNumber("100")
        Calculator.addOperator("+")
        Calculator.addNumber("200")
        Calculator.addOperator("-")
        Calculator.addNumber("300")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, 0.00, 0.00)
        println("test_evaluate passed!")
    }

    // dodani testovi, testiranje evaluacije, mnozenje
    @Test
    fun test_evaluate_multiplication() {
        Calculator.reset()
        Calculator.addNumber("100")
        Calculator.addOperator("*")
        Calculator.addNumber("200")
        Calculator.addOperator("*")
        Calculator.addNumber("0")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, 0.00, 0.00)

        // dodani testovi, testiranje evaluacije, mnozenje, niza operacija(+ ili -) na kraju
        // (radilo probleme zbog nacina obrade expressiona)
        Calculator.reset()
        Calculator.addNumber("3")
        Calculator.addOperator("+")
        Calculator.addNumber("3")
        Calculator.addOperator("*")
        Calculator.addNumber("3")
        Calculator.addOperator("*")
        Calculator.addNumber("3")
        Calculator.addOperator("-")
        Calculator.addNumber("30")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, 0.00, 0.00)

        println("test_evaluate_multiplication passed!")
    }



    // dodani testovi, testiranje evaluacije, dijeljenje
    @Test
    fun test_evaluate_division() {
        Calculator.reset()
        Calculator.addNumber("33")
        Calculator.addOperator("/")
        Calculator.addNumber("3")
        Calculator.addOperator("+")
        Calculator.addNumber("33")
        Calculator.addOperator("/")
        Calculator.addNumber("3")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, 22.00, 0.00)

        //dodani testovi, dijeljenje nule s brojem
        Calculator.reset()
        Calculator.addNumber("999")
        Calculator.addOperator("/")
        Calculator.addNumber("3")
        Calculator.addOperator("+")
        Calculator.addNumber("0")
        Calculator.addOperator("/")
        Calculator.addNumber("3")
        Calculator.addOperator("-")
        Calculator.addNumber("333")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, 0.00, 0.00)

        //dodani testovi, dijeljenje broja s nulom
        Calculator.reset()
        Calculator.addNumber("3")
        Calculator.addOperator("/")
        Calculator.addNumber("0")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, Double.POSITIVE_INFINITY, 0.00)
        println("test_evaluate_division passed!")
    }

    //dodani testovi, testiranje evaluacije, "kompleksni izraz"
    @Test
    fun test_evaluate_expression_1() {
        Calculator.reset()
        Calculator.addNumber("25")
        Calculator.addOperator("*")
        Calculator.addNumber("3")
        Calculator.addOperator("+")
        Calculator.addNumber("225")
        Calculator.addOperator("/")
        Calculator.addNumber("3")
        Calculator.addOperator("+")
        Calculator.addNumber("150")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, 300.00, 0.00)
        println("test_evaluate_expression_1 passed!")
    }

    //dodani testovi, testiranje evaluacije, "kompleksni izraz"
    @Test
    fun test_evaluate_expression_2() {
        Calculator.reset()
        Calculator.addNumber("999")
        Calculator.addOperator("/")
        Calculator.addNumber("3")
        Calculator.addOperator("+")
        Calculator.addNumber("3")
        Calculator.addOperator("/")
        Calculator.addNumber("0")
        Calculator.addOperator("-")
        Calculator.addNumber("333")
        Calculator.evaluate()
        Assert.assertEquals(Calculator.result, Double.POSITIVE_INFINITY, 0.00)
        println("test_evaluate_expression_2 passed!")
    }
}