package lecture4.ruazosa.fer.hr.calculator

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CalculatorViewModel: ViewModel(){
    private val calculatorModel = MutableLiveData<Calculator>()
    private var calculatorResult: MutableLiveData<String> = MutableLiveData<String>()

    init {
        calculatorModel.value = Calculator
        calculatorResult.value = "0"
    }

    fun getResult(): MutableLiveData<String> {
        return calculatorResult
    }

    fun addNumber(number: String){
        calculatorModel.value?.addNumber(number)
    }

    fun addOperator(operator: String){
        calculatorModel.value?.addOperator(operator)
    }

    fun evaluate(){
        calculatorModel.value?.evaluate()
        calculatorResult.value = calculatorModel?.value?.result.toString()
    }

    fun reset(){
        calculatorModel.value?.reset()
        calculatorResult.value = calculatorModel?.value?.result.toString()
    }
}