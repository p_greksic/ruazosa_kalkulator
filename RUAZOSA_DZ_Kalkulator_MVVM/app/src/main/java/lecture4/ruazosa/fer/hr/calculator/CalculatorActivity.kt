package lecture4.ruazosa.fer.hr.calculator

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_calculator.*


class CalculatorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)
        // zakomentirani su razni pokusaji definiranja observera i viewmodela
        //val viewModelProvider = ViewModelProviders.of(this)[CalculatorViewModel::class.java]
        //val calculatorViewModelTest = viewModelProvider.get(CalculatorViewModel::class.java)
        /*val calculatorObserver: Observer<Calculator> = Observer{
            result_view?.text = it.result.toString()
        }*/

        // inicijaliziraj ViewModel
        val calculatorViewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(CalculatorViewModel::class.java)
        // definiraj observer akciju
        val resultObserver: Observer<String> = Observer {
            result_view?.text = it.toString()
        }
        //definiraj observer akciju
        //calculatorViewModel.calculatorModel.observe(this, calculatorObserver)
        /*calculatorViewModel.calculatorModel.observe(this, Observer { calculatorModel ->
            //azuriraj prikaz rezultata na grafickom sucelju
            result_view?.text = calculatorModel?.result.toString()
        })*/
        calculatorViewModel.getResult().observe(this, resultObserver)
        // pomocni ispis kod debuggiranja
        /*
        if(calculatorViewModel.getResult().hasObservers()){
            Log.d("Calculator_DEBUG", "CalculatorModel observer is set!")
        }else{
            Log.d("Calculator_DEBUG", "CalculatorModel observer is not set!")
        }
        val calculatorModelTest = calculatorViewModel.getResult()
        if(calculatorModelTest === null) {
            Log.d("Calculator_DEBUG", "CalculatorModel is null!")
        }else{
            Log.d("Calculator_DEBUG", "CalculatorModel is not null!")
        }
        */

        var inOperatorMode: Boolean = false
        var inResultMode: Boolean = false
        // varijabla LastOperator sluzi za odluciavnje koji ce se neutralni broj dodati na kraju izraza
        // u slucaju + ili - to je 0 a u slucaju * ili / to je 1
        var lastOperator: String? = null
        var inResetMode: Boolean = true

        val numericButtonClicked = {view: View ->

            val buttonValue = (view as Button).text.toString()
            if (!inResetMode && !inOperatorMode && !inResultMode) {
                    result_view?.text = result_view?.text.toString() + buttonValue
            } else {
                inResetMode = false
                inOperatorMode = false
                inResultMode = false
                result_view?.text = buttonValue
            }
        }

        val operatorButtonClicked = {view: View ->

            val buttonValue = (view as Button).text.toString()
            //if(calculatorViewModel != null) {}  // always true, pa maknuto
            calculatorViewModel.addNumber(result_view?.text.toString())
            calculatorViewModel.addOperator(buttonValue)
            inOperatorMode = true
            // azuriraj vrijednost zadnjeg operatora
            lastOperator = buttonValue

        }


        button_reset?.setOnClickListener {

            //if(calculatorViewModel != null) {} // always true, pa maknuto
            calculatorViewModel.reset()
            inResetMode = true

        }

        button_comma?.setOnClickListener {

            if (!result_view?.text.toString().contains(char = '.')) {
                result_view?.text = result_view?.text.toString() + ".";
            }

        }

        button_zero.setOnClickListener(numericButtonClicked)
        button_one.setOnClickListener(numericButtonClicked)
        button_two.setOnClickListener(numericButtonClicked)
        button_three.setOnClickListener(numericButtonClicked)
        button_four.setOnClickListener(numericButtonClicked)
        button_five.setOnClickListener(numericButtonClicked)
        button_six.setOnClickListener(numericButtonClicked)
        button_seven.setOnClickListener(numericButtonClicked)
        button_eight.setOnClickListener(numericButtonClicked)
        button_nine.setOnClickListener(numericButtonClicked)

        button_plus.setOnClickListener(operatorButtonClicked)
        button_minus.setOnClickListener(operatorButtonClicked)
        button_divide.setOnClickListener(operatorButtonClicked)
        button_multiply.setOnClickListener(operatorButtonClicked)

        button_evaluate.setOnClickListener {

            //if(calculatorViewModel != null) {} // always true, pa maknuto
            if (inOperatorMode) {
                if (lastOperator.isNullOrBlank()) {
                    // nema zadnjeg operatora, do nothing
                } else if (lastOperator.equals("/") || lastOperator.equals("*")) {
                    // neutralni element za mnozenje i dijeljenje
                    calculatorViewModel.addNumber("1")
                } else if (lastOperator.equals("+") || lastOperator.equals("-")) {
                    // neutralni element za zbrajanje i oduzimanje
                    calculatorViewModel.addNumber("0")
                } else {
                    // nema zadnjeg operatora, do nothing
                }
                inOperatorMode = false
            } else if(lastOperator == null){
                calculatorViewModel.addNumber(result_view?.text.toString())
                calculatorViewModel.addOperator("+")
                calculatorViewModel.addNumber("0")
            }else {
                calculatorViewModel.addNumber(result_view?.text.toString())
            }
            calculatorViewModel.evaluate()
            inResultMode = true
            lastOperator = null

        }

    }
}
