package lecture4.ruazosa.fer.hr.calculator

import android.util.Log

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {

    var result: Double = 0.0
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    fun reset() {
        result = 0.0
        expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "/" -> expression.add(operator)
            "*" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun evaluate() {
        /*
        * Ideja: dvije petlje
        * Prva obavlja mnozenja i dijeljenje dok god ih ima
        * Druga obavlja zbrajanja i oduzimanja dok god ih ima
        */
        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }

        // lista u koju se spremaju rezultati operacija veceg prioriteta i ostali elementi originane liste
        var expressionLowerRank: MutableList<String> = mutableListOf()
        // varijabla za spremanje rezultata operacija * i /
        var res: Double = 0.0
        // index trenutnog elementa u listi
        var index: Int = 1
        // oznaka da je operacija visokog prioriteta u tijeku (koristi se za uzastopne operacije * i /)
        var higherRankOpInProgress: Boolean = false

        //prvo prodji kroz listu epression i izvedi operacije viseg prioriteta, operacije * i /
        do {
            if(expression[index].equals("*") || expression[index].equals("/")){
                if(higherRankOpInProgress.equals(true)){
                    when(expression[index]) {
                        "*" -> res = res * expression[index+1].toDouble()
                        "/" -> res = res / expression[index+1].toDouble()
                    }
                }else{
                    higherRankOpInProgress = true
                    when(expression[index]) {
                        "*" -> res = expression[index-1].toDouble() * expression[index+1].toDouble()
                        "/" -> res = expression[index-1].toDouble() / expression[index+1].toDouble()
                    }
                }
            }else{
                if(higherRankOpInProgress.equals(true)){
                    higherRankOpInProgress = false
                    expressionLowerRank.add(res.toString())
                    expressionLowerRank.add(expression[index])
                }else {
                    expressionLowerRank.add(expression[index - 1])
                    expressionLowerRank.add(expression[index])

                }
                // ako smo na zadnjem operatoru u listi
                // aka index je jednak expression.count()-2
                // spremi i desni(zadnji) element u listi
                if(index.equals(expression.count()-2)){
                    expressionLowerRank.add(expression[index+1])
                }
            }
            index+=2
        }while(index < expression.count()-1)
        //provjeri je li operacija viseg prioriteta bila u tijeku, ako je spremi rezultat
        if(higherRankOpInProgress.equals(true)){
            higherRankOpInProgress = false
            expressionLowerRank.add(res.toString())
        }
        /*
        // ispis liste u svrhu debuggiranja
        // Pripremi sadrzaj expressionLowerRank liste za ispis
        var expressionLowerRankContent: String = expressionLowerRank[0]
        for(i in 1..expressionLowerRank.count()-1){
            expressionLowerRankContent = expressionLowerRankContent + expressionLowerRank[i]
        }
        // Ispisi sadrzaj expressionLowerRank liste
        Log.d("Calculator_DEBUG", expressionLowerRankContent)
        */
        //zatim prodji kroz listu i izvedi operacije manjeg prioriteta, + i -
        result = expressionLowerRank[0].toDouble()

        for(i in 1..expressionLowerRank.count()- 1 step 2) {
            when(expressionLowerRank[i]) {
                "+" -> result = result + expressionLowerRank[i+1].toDouble()
                "-" -> result = result - expressionLowerRank[i+1].toDouble()
            }
        }
        // "resetiraj" expression
        expression = mutableListOf()
    }
}