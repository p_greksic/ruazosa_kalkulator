package lecture4.ruazosa.fer.hr.calculator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_calculator.*


class CalculatorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)

        var inOperatorMode: Boolean = false
        var inResultMode: Boolean = false
        // varijabla LastOperator sluzi za odluciavnje koji ce se neutralni broj dodati na kraju izraza
        // u slucaju + ili - to je 0 a u slucaju * ili / to je 1
        var lastOperator: String? = null

        val numericButtonClicked = {view: View ->
            val buttonValue = (view as Button).text.toString()
            if (!result_view?.text.toString().equals("0") && !inOperatorMode && !inResultMode) {
                    result_view?.text = result_view?.text.toString() + buttonValue
            } else {
                inOperatorMode = false
                inResultMode = false
                result_view?.text = buttonValue
            }
        }

        val operatorButtonClicked = {view: View ->
            val buttonValue = (view as Button).text.toString()
            Calculator.addNumber(result_view?.text.toString())
            Calculator.addOperator(buttonValue)
            inOperatorMode = true
            // azuriraj vrijednost zadnjeg operatora
            lastOperator = buttonValue
        }


        button_reset?.setOnClickListener {
            Calculator.reset()
            result_view?.setText("0")
        }

        button_comma?.setOnClickListener {
            if (!result_view?.text.toString().contains(char = '.')) {
                result_view?.text = result_view?.text.toString() + ".";
            }
        }

        button_zero.setOnClickListener(numericButtonClicked)
        button_one.setOnClickListener(numericButtonClicked)
        button_two.setOnClickListener(numericButtonClicked)
        button_three.setOnClickListener(numericButtonClicked)
        button_four.setOnClickListener(numericButtonClicked)
        button_five.setOnClickListener(numericButtonClicked)
        button_six.setOnClickListener(numericButtonClicked)
        button_seven.setOnClickListener(numericButtonClicked)
        button_eight.setOnClickListener(numericButtonClicked)
        button_nine.setOnClickListener(numericButtonClicked)

        button_plus.setOnClickListener(operatorButtonClicked)
        button_minus.setOnClickListener(operatorButtonClicked)
        button_divide.setOnClickListener(operatorButtonClicked)
        button_multiply.setOnClickListener(operatorButtonClicked)

        button_evaluate.setOnClickListener {
            if (inOperatorMode) {
                if (lastOperator.isNullOrBlank()) {
                    // nema zadnjeg operatora, do nothing
                } else if (lastOperator.equals("/") || lastOperator.equals("*")) {
                    // neutralni element za mnozenje i dijeljenje
                    Calculator.addNumber("1")
                } else if (lastOperator.equals("+") || lastOperator.equals("-")) {
                    // neutralni element za zbrajanje i oduzimanje
                    Calculator.addNumber("0")
                } else {
                    // nema zadnjeg operatora, do nothing
                }
                inOperatorMode = false
            } else if(lastOperator == null){
                // ukoliko je izraz samo jedan broj, dakle nije dodan operator
                // dodaj operator + i 0 kako bi se izbjegao error u evaluate
                // u dijelu koji obradjuje operatore viseg prioriteta funkciji!
                Calculator.addNumber(result_view?.text.toString())
                Calculator.addOperator("+")
                Calculator.addNumber("0")
            }else {
                Calculator.addNumber(result_view?.text.toString())
            }

            Calculator.evaluate()
            result_view?.text = Calculator.result.toString()
            inResultMode = true
            lastOperator = null
            Calculator.reset()
        }


    }
}
