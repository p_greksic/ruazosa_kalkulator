# RUAZOSA_Kalkulator

DZ uz 3. predavanje iz predmeta Razvoj usluga i aplikacija za operacijski sustav Android.

Student: Petar Grekšić

Struktura repozitorija:
	
	RUAZOSA_DZ_Kalkulator --> Rješenje domaće zadaće bez korištenja obrazca MVVM
	
	RUAZOSA_DZ_Kalkulator_MVVM --> Rješenje domaće zadaće uz korištenje obrazca MVVM
	